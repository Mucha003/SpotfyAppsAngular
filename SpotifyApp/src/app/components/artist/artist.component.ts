import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {

  artiste:any = {};
  ListTracks:any[] = [];
  loadingArtist: boolean;

  constructor(private route:ActivatedRoute,
              private http:SpotifyService) { 
    this.route.params.subscribe(pm => {

      this.getArtist(pm['id']);
      this.getTopTracks(pm['id']);

    });
  }

  ngOnInit() {
  }

  getArtist(id:string){
    this.loadingArtist = true;
    this.http.getArtistById(id).subscribe(x => {
      this.artiste = x;
      this.loadingArtist = false;
    });
  }

  getTopTracks(id:string){
    this.http.getTpTracks(id).subscribe((x:any[]) => {
      this.ListTracks = x;
      console.log(x);
      this.loadingArtist = false;
    });
  }
}
