import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  ListnewsMusic: any[] = [];
  loading: boolean;
  errorChargement: boolean;
  msgError:string;

  constructor(private http:SpotifyService) {
    this.errorChargement = false; 
    this.getNewMusic();
  }

  getNewMusic(){
    this.loading = true;
    this.http.getNewReleases().subscribe((data: any ) => { 
      this.ListnewsMusic = data;
      this.loading = false;
    },(errorServ) => {
      this.loading = false;
      this.errorChargement = true;
      this.msgError = errorServ.error.error.message;
    });
  }

  ngOnInit() {
  }

}
