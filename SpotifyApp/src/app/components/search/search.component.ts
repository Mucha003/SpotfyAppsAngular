import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  artistes:any[] = [];
  loading:boolean;

  constructor(private http:SpotifyService) {
  }

  searchArtist(motSearch: string){
    this.loading = true;
    this.http.getArtist(motSearch).subscribe((data: any ) => { 
      this.artistes = data;
      this.loading = false;
    });
  }

  ngOnInit() {
  }

}
