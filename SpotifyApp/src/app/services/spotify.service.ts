import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http:HttpClient) { }

  getUrl( query: string){
    const URL = `https://api.spotify.com/v1/${query}`;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQCpo5upDlfmhOuFUFOjupKTwRO-qwe064NGyGN7TRbwG4I5ms8KORYl2lIOr6YSpNoc9rE0J7KeFVx7mBw'
    });

    return this.http.get(URL, { headers });
  }

  getNewReleases(){
    let qury = 'browse/new-releases?limit=20';
    return this.getUrl(qury).pipe(map(data => data['albums'].items));
  }

  getArtist(ArtistSearch:string){
    let qury = `search?q=${ArtistSearch}&type=artist&limit=15`;
    return this.getUrl(qury).pipe(map(resp => resp['artists'].items));
  }

  getArtistById(id:string){
    return this.getUrl(`artists/${id}`);
  }

  getTpTracks(id:string){
    return this.getUrl(`artists/${id}/top-tracks?country=us`)
                .pipe(map(data => data['tracks']));
  }





}


